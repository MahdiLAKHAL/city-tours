import React, { useState, useEffect } from 'react';
const url = 'https://course-api.com/react-tours-project';

function Tour({ id, image, info, name, price, removeTour }) {
  const [readmore, setReadmore] = useState(false);

  return (
    <article key={id} className='single-tour'>
      <img src={image} alt={name} />
      <footer>
        <div className='tour-info'>
          <h4>{name}</h4>
          <h4 className='tour-price'>${price}</h4>
        </div>
        <p>{readmore ? info : `${info.substring(0, 200)}...`}</p>
        <button onClick={() => setReadmore(!readmore)}>
          {readmore ? 'show less' : 'read more'}
        </button>
        <button className='delete-btn' onClick={() => removeTour(id)}>
          not interested
        </button>
      </footer>
    </article>
  );
}

export default Tour;
